package main

import (
	"fmt"
	"zaif_api_client/pkg/client"
)

func main() {
	ret, err := client.GetInfomation()
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("ret:%v\n", ret)
	}
}
