package main

import (
	"fmt"
	"zaif_api_client/pkg/client"

	"github.com/dustin/go-humanize"
)

func main() {
	ret, err := client.GetTicker("btc_jpy")
	btc_jpy := int64(ret.Last)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("ret:%v\n", humanize.Comma(btc_jpy))
	}
}
