package client

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
)

type ResultActiveOrder struct {
	Success int            `json:"success"`
	Error   string         `json:"error"`
	Return  AllActiveOrder `json:"return"`
}
type AllActiveOrder struct {
	TokenActiveOrders map[int64]ActiveOrder `json:"token_active_orders"`
	ActiveOrders      map[int64]ActiveOrder `json:"active_orders"`
}

type ActiveOrder struct {
	Amount       float64     `json:"amount"`
	Action       string      `json:"action"`
	Timestamp    json.Number `json:"timestamp"`
	Comment      string      `json:"comment"`
	CurrencyPair string      `json:"currency_pair"`
	Price        float64     `json:"price"`
}

func GetActiveOrders() (map[int64]ActiveOrder, error) {
	nonce := getNonce()
	body := fmt.Sprintf("nonce=%.9f&method=active_orders&is_token_both=true", nonce)
	req := createReqest(body)
	out, err := SendRequest(req)
	if err != nil {
		log.Printf("GetInfomation: %s", err)
		return map[int64]ActiveOrder{}, err
	}

	res, err := NewActiveOrders(out)
	if err != nil {
		log.Printf("GetInfomation: %s", err)
		return map[int64]ActiveOrder{}, err
	}
	return merge(res.TokenActiveOrders, res.ActiveOrders), nil
}

func merge(m1, m2 map[int64]ActiveOrder) map[int64]ActiveOrder {
	ans := map[int64]ActiveOrder{}

	for k, v := range m1 {
		ans[k] = v
	}
	for k, v := range m2 {
		ans[k] = v
	}
	return (ans)
}

func GetActiveOrdersFromName(name string) (map[int64]ActiveOrder, error) {
	nonce := getNonce()
	body := fmt.Sprintf("nonce=%.9f&method=active_orders&currency_pair=%s", nonce)
	req := createReqest(body)
	out, err := SendRequest(req)
	if err != nil {
		log.Printf("GetInfomation: %s", err)
		return map[int64]ActiveOrder{}, err
	}
	res, err := NewActiveOrders(out)
	if err != nil {
		log.Printf("GetInfomation: %s", err)
		return map[int64]ActiveOrder{}, err
	}
	return merge(res.TokenActiveOrders, res.ActiveOrders), nil
}

func NewActiveOrders(jsonBytes []byte) (AllActiveOrder, error) {
	result := ResultActiveOrder{}
	if err := json.Unmarshal(jsonBytes, &result); err != nil {
		return AllActiveOrder{}, err
	}
	if result.Success != 1 {
		return AllActiveOrder{}, errors.New(result.Error)
	}
	data := result.Return
	return data, nil
}
