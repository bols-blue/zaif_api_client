package client

import (
	"fmt"
	"gitlab.com/bols-blue/zaif_api_client/pkg/currency"
	"log"
	"os"
	"testing"
	"time"
)

func TestMain(m *testing.M) {
	ZaifKey = os.Getenv("ZAIF_KEY")
	ZaifSct = os.Getenv("ZAIF_SCT")
	fmt.Println("key:" + ZaifKey + " sct:" + ZaifSct)
	pythonDir = "../python/"
	os.Exit(m.Run())
}

func TestGetTradeHistoryFromCurrencyPair(t *testing.T) {
	lastHistoryId = 0
	resp, err := GetZaifHistoryFromCurrencyPair(time.Now().Unix()-12000, "bch_btc")
	if err != nil {
		fmt.Printf("%v\n", err)
	} else {
		fmt.Printf("%v\n", resp)
	}

}

func TestGetTradeHistory(t *testing.T) {
	lastHistoryId = 0
	_, err := GetZaifHistory(time.Now().Unix())
	if err != nil {
		fmt.Printf("%v\n", err)
	} else {
		fmt.Printf("success\n")
	}

}

func TestSigen(t *testing.T) {
	actial := "18c36579a3597813940191196f03a03c4ec2b47d5a7bebf5419776e7667dbece513e6ea6162618c5ef785b3765a72130088c679f9aa092edb75370f233332f84"
	in := []byte(`nonce=1504559281.084844&method=trade_history`)
	result := hamacSha512(in)
	if result != actial {
		log.Fatalf("result:%s actial:%s\n", result, actial)
	}

}

func readFile(path string) []byte {
	bufSize := 1024 * 10
	file, err := os.Open(path)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	buf := make([]byte, bufSize)
	for {
		n, err := file.Read(buf)
		if n == 0 {
			break
		}
		if err != nil {
			fmt.Println(err)
			break
		}
		return buf[:n]
	}
	return buf
}

func TestOrder(t *testing.T) {
	order := &ZaifOrder{
		comment: "test", Name: "bch_btc",
		action: "bid", price: 0.03,
		unit: 1,
	}
	fmt.Printf("%v\n", order)
	time.Sleep(11 * time.Second)
	order.createOrder()
}

func TestPublicOrder(t *testing.T) {
	set := currency.CurrencySet{Unit: 100}
	order_old := &ZaifOrder{}
	order := order_old.CreateOrder(set, "xem_btc", 0.00018200, "ask")
	fmt.Printf("%v\n", order)
	var checkIsTooNew int = 0
	if order.IsTooNew() {
		time.Sleep(5 * time.Second)
		checkIsTooNew++
	}
	if order.IsTooNew() {
		time.Sleep(6 * time.Second)
		checkIsTooNew++
	}
	order.CancelOrder()
	if checkIsTooNew != 2 {
		t.Fatal("IsTooNew is miss")
	}
}

func TestGetLastId(t *testing.T) {
	lastHistoryId = 0
	GetLastHistoryId()
	if lastHistoryId == 0 {
		t.Errorf("error lastHistoryId is 0")
	}
	fmt.Printf("lastHistoryId:%d\n", lastHistoryId)
	fmt.Printf("lastHistoryIdMap:%v\n", lastHistoryIdMap)
}

func TestGetTradeHistoryNodata(t *testing.T) {
	GetLastHistoryId()
	resp, err := GetZaifHistory(time.Now().UTC().Unix() - 3600*2)
	if err != nil {
		fmt.Printf("%v\n", err)
	} else {
		fmt.Printf("last History:%s\n", resp)
	}

}
