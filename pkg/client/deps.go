package client

import (
	"encoding/json"
	"gitlab.com/bols-blue/zaif_api_client/pkg/models"
	"net/http"
)

//https://api.zaif.jp/api/1/depth/btc_jpy

func Depth(pairName string) (*models.DepthData, error) {
	uri := "http://api.zaif.jp/api/1/depth/" + pairName
	req, _ := http.NewRequest("GET", uri, nil)

	client := new(http.Client)
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	target := new(models.DepthData)
	err = json.NewDecoder(resp.Body).Decode(target)
	return target, err
}
