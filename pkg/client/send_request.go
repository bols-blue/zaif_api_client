package client

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha512"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"os"
	"time"
)

var debugMode bool = false
var ZaifKey string
var ZaifSct string

func hamacSha512(data []byte) string {
	hash := hmac.New(sha512.New, []byte(ZaifSct))
	hash.Write([]byte(data))

	return fmt.Sprintf("%x", hash.Sum(nil))
}

func CreateReqest(body string) *http.Request {
	return createReqest(body)
}
func createReqest(body string) *http.Request {
	url := "https://api.zaif.jp/tapi"
	byteBody := []byte(body)
	sigin := hamacSha512(byteBody)

	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(byteBody))
	headers := http.Header{
		"key":  []string{ZaifKey},
		"sign": []string{sigin},
	}

	req.Header = headers
	return req
}

func SendRequest(req *http.Request) ([]byte, error) {
	client := new(http.Client)
	if debugMode == true {
		dump, err := httputil.DumpRequestOut(req, true)
		if err != nil {
			log.Fatal(err)
		} else {
			log.Printf("%q", dump)
		}
	}
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("SendRequest err:%s", err)
		return nil, err
	}
	b, err := ioutil.ReadAll(resp.Body)
	if err == nil {
		return b, nil
	} else {
		return nil, err
	}
}

func getNonce() float64 {
	return float64(time.Now().UnixNano()) / 1000000000
}

func init() {
	ZaifKey = os.Getenv("ZAIF_KEY")
	ZaifSct = os.Getenv("ZAIF_SCT")

	GetLastHistoryId()
}
