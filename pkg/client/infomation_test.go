package client

import (
	"fmt"
	"testing"
)

func TestGetInfomation(t *testing.T) {
	_, err := GetInfomation()
	if err != nil {
		t.Errorf("%v\n", err)
	}
}

func TestNewInfomation(t *testing.T) {
	s := readFile("../test/info.json")
	fmt.Printf("%s\n", s)
	_, err := NewInfomation(s)
	if err != nil {
		t.Errorf("%v\n", err)
	}

}

func TestNewBalanceSetting(t *testing.T) {
	s := readFile("../test/balance.json")
	_, err := NewBalanceSetting(s)
	if err != nil {
		t.Errorf("%v\n", err)
	}
}

func TestCheckBalance(t *testing.T) {
	s := readFile("../test/balance.json")
	balance, err := NewBalanceSetting(s)
	if err != nil {
		t.Errorf("%v\n", err)
	}
	s = readFile("../test/info.json")
	info, err := NewInfomation(s)
	if err != nil {
		t.Errorf("%v\n", err)
	}
	info, err = GetInfomation()
	if err != nil {
		t.Errorf("%v\n", err)
	}

	m := CheckBalance(info, balance)
	fmt.Printf("CheckBalance: %v\n", m)
}
