package client

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"time"
)

func GetZaifHistory(history_time int64) (map[int64]History, error) {
	nonce := getNonce()
	body := fmt.Sprintf("method=trade_history&nonce=%.9f&since=%d&from_id=%d", nonce, history_time, lastHistoryId+1)
	req := createReqest(body)
	out, err := SendRequest(req)
	if err != nil {
		log.Printf("GetZaifHistory: %s", err)
	}
	return NewHistoryMap(out)
}

func GetZaifHistoryFromCurrencyPair(history_time int64, pair string) (map[int64]History, error) {
	nonce := getNonce()
	lastId := lastHistoryIdMap[pair]
	body := fmt.Sprintf("method=trade_history&currency_pair=%s&nonce=%.9f&since=%d&from_id=%d", pair, nonce, history_time, lastId+1)
	log.Println(body)
	req := createReqest(body)
	out, err := SendRequest(req)
	if err != nil {
		log.Printf("GetZaifHistory: %s", err)
	}
	return NewHistoryMap(out)
}

type ResultHistory struct {
	Success int               `json:"success"`
	Error   string            `json:"error"`
	Return  map[int64]History `json:"return"`
}

type History struct {
	Amount       float64     `json:"amount"`
	Bonus        float64     `json:"bonus"`
	Fee          float64     `json:"fee"`
	Action       string      `json:"action"`
	Timestamp    json.Number `json:"timestamp"`
	FeeAmount    float64     `json:"fee_amount"`
	Comment      string      `json:"comment"`
	CurrencyPair string      `json:"currency_pair"`
	Price        float64     `json:"price"`
	YourAction   string      `json:"your_action"`
}

func (his History) String() string {
	return fmt.Sprintf("action:%s amount:%f Price:%.8f CurrencyPair:%s comment:%s\n",
		his.YourAction, his.Amount-his.FeeAmount, his.Price, his.CurrencyPair, his.Comment)
}

var lastHistoryIdMap map[string]int64 = map[string]int64{}
var lastHistoryId int64

var pythonDir string

func init() {
	ZaifKey = os.Getenv("ZAIF_KEY")
	ZaifSct = os.Getenv("ZAIF_SCT")

	GetLastHistoryId()
}

func updateLastHistoryId(logs map[int64]History) {
	for k, history := range logs {
		if lastHistoryIdMap[history.CurrencyPair] < k {
			lastHistoryIdMap[history.CurrencyPair] = k
		}
		if lastHistoryId < k {
			lastHistoryId = k
		}
	}
}

func GetLastHistoryId() {
	logs, err := GetZaifHistory(time.Now().UTC().Unix() - 3600*24)
	if err != nil {
		log.Printf("%v", err)
	} else {
		updateLastHistoryId(logs)
	}
}

func NewHistoryMap(jsonBytes []byte) (map[int64]History, error) {
	result := ResultHistory{}
	if err := json.Unmarshal(jsonBytes, &result); err != nil {
		return nil, err
	}

	if result.Success != 1 {
		return map[int64]History{}, errors.New(result.Error)
	}

	data := result.Return
	updateLastHistoryId(data)
	return data, nil
}
