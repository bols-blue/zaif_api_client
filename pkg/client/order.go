package client

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/bols-blue/zaif_api_client/pkg/currency"
	"gitlab.com/bols-blue/zaif_api_client/pkg/interfaces"
	"log"
	"time"
)

type ZaifOrder struct {
	orderId   int64
	lastNo    int
	price     float64
	unit      float64
	action    string
	Name      string
	comment   string
	timeStamp int64
}

func (order *ZaifOrder) GetID() int64 {
	return order.orderId
}

func (order *ZaifOrder) SetID(id int64) {
	order.orderId = id
}

func (order *ZaifOrder) String() string {
	if order.Name != "btc_jpy" {
		return fmt.Sprintf("Order{orderId:%d,price:%.8f,unit:%.0f,action:%s,name:%s,comment:%s}",
			order.orderId, order.price, order.unit, order.action, order.Name, order.comment)
	} else {
		return fmt.Sprintf("Order{orderId:%d,price:%f,unit:%.8f,action:%s,name:%s,comment:%s}",
			order.orderId, order.price, order.unit, order.action, order.Name, order.comment)
	}
}

type ZaifOrderResponce struct {
	Funds    map[string]float64 `json:"funds"`
	Received float64            `json:"received"`
	Remains  float64            `json:"remains"`
	OrderId  int64              `json:"order_id"`
}

func (zor ZaifOrderResponce) String() string {
	return fmt.Sprintf("ZaifOrderResponce{OrderId:%d,Received:%f,Remains:%f}",
		zor.OrderId, zor.Received, zor.Remains)
}

type ZaifOrderHttpResponce struct {
	Success int               `json:"success"`
	Return  ZaifOrderResponce `json:"return"`
}

func (zo *ZaifOrder) createOrder() {
	var unit_str string
	var price_str string
	if zo.Name == "btc_jpy" {
		unit_str = fmt.Sprintf("%.4f", zo.unit)
		price_str = fmt.Sprintf("%.0f", zo.price)
	} else if zo.Name == "bch_jpy" {
		unit_str = fmt.Sprintf("%.4f", zo.unit)
		price_str = fmt.Sprintf("%.0f", zo.price)
	} else if zo.Name == "bch_btc" {
		unit_str = fmt.Sprintf("%.4f", zo.unit)
		price_str = fmt.Sprintf("%.4f", zo.price)
	} else {
		unit_str = fmt.Sprintf("%.0f", zo.unit)
		price_str = fmt.Sprintf("%.8f", zo.price)
	}
	nonce := float64(time.Now().UnixNano()) / 1000000000
	body := fmt.Sprintf("method=trade&nonce=%.9f&currency_pair=%s&action=%s&price=%s&amount=%s&comment=%s", nonce, zo.Name, zo.action, price_str, unit_str, zo.comment)
	data, err := zo.createOrderRequest(body)
	//out, err := exec.Command("python", pythonDir+"order.py", zo.Name, zo.action, price_str, unit_str, zo.comment).Output()
	if err != nil {
		log.Printf("command failed: %v", err)
		zo.orderId = 0
		zo.price = 0
	} else {
		zo.orderId = data.OrderId
	}
}

func (zo *ZaifOrder) createOrderRequest(body string) (ZaifOrderResponce, error) {
	req := createReqest(body)
	out, err := SendRequest(req)
	if err != nil {
		log.Printf("Error createOrderRequest: %s", err)
	}
	var data ZaifOrderHttpResponce
	if err := json.Unmarshal(out, &data); err != nil {
		log.Printf("%s\n", out)
		return data.Return, err
	}
	if data.Success == 0 {
		return data.Return, errors.New(string(out))
	}
	zo.timeStamp = time.Now().UTC().Unix()
	return data.Return, nil

}

func (zo *ZaifOrder) IsTooNew() bool {
	if zo.timeStamp < time.Now().UTC().Unix()-10 {
		return false
	} else {
		log.Printf("%s order too new", zo.Name)
		return true
	}
}

var cancelRetryChan (chan ZaifOrder)

func CancelTrade(zo ZaifOrder) {
	for {
		time.Sleep(100 * time.Millisecond)
		if !zo.IsTooNew() {
			break
		}
	}
	zo.cancelOrderRequest()
}
func (zo *ZaifOrder) CancelOrderFromID(id int64, name string) {
	zo.orderId = id
	zo.Name = name
	zo.CancelOrder()
}
func (zo *ZaifOrder) ClearOrder() {
	zo.orderId = 0
	zo.price = 0
	zo.unit = 0
}
func (zo *ZaifOrder) CancelOrder() {
	if zo.orderId != 0 {
		out, err := zo.cancelOrderRequest()
		if err != nil {
			go CancelTrade(*zo)
		} else {
			log.Printf("%s\n", out)
		}
	}
	zo.ClearOrder()
}

type ZaifCancelOrderHttpResponce struct {
	Success int               `json:"success"`
	Return  ZaifOrderResponce `json:"return"`
	Error   string            `json:"error"`
}

func (zo *ZaifOrder) cancelOrderRequest() (ZaifOrderResponce, error) {
	nonce := float64(time.Now().UnixNano()) / 1000000000
	id := fmt.Sprintf("%d", zo.orderId)
	var body string
	if zo.Name == "bch_btc" {
		body = fmt.Sprintf("method=cancel_order&nonce=%.9f&order_id=%s&is_token=true", nonce, id)
	} else {
		body = fmt.Sprintf("method=cancel_order&nonce=%.9f&order_id=%s", nonce, id)
	}
	req := createReqest(body)
	out, err := SendRequest(req)
	if err != nil {
		log.Printf("Error cancelOrderRequest: %s", err)
	}
	var data ZaifCancelOrderHttpResponce
	if err := json.Unmarshal(out, &data); err == nil {
		log.Printf("%s\n", out)
		return ZaifOrderResponce{}, err
	}
	if data.Success == 0 {
		if data.Error == "order not found" {
			zo.orderId = 0
		}
		return data.Return, errors.New(data.Error)
	}
	return data.Return, nil
}

func (zo *ZaifOrder) CreateOrder(set currency.CurrencySet, name string, price float64, tradeType string) interfaces.Order {
	unit := set.Unit
	zo.UpdateOrderData(unit, name, price, tradeType, "auto")
	zo.createOrder()
	return zo
}

func (zo *ZaifOrder) UpdateOrderData(unit float64, name string, price float64, tradeType string, comment string) {
	zo.Name = name
	zo.price = price
	zo.unit = unit
	zo.action = tradeType
	zo.comment = comment
}
func (zo *ZaifOrder) CreateRawOrder(unit float64, name string, price float64, tradeType string, comment string) interfaces.Order {
	zo.UpdateOrderData(unit, name, price, tradeType, comment)
	zo.createOrder()
	return zo
}

func (order *ZaifOrder) IsNoOrder() bool {
	return order.orderId == 0
}

func (order *ZaifOrder) GetPrice() float64 {
	return order.price
}
