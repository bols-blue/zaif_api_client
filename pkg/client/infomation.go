package client

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math"
)

type ResultInfomation struct {
	Success int        `json:"success"`
	Error   string     `json:"error"`
	Return  Infomation `json:"return"`
}

type Moneies map[string]float64

type Rights struct {
	Info          int32 `json:"info"`
	Trade         int32 `json:"trade"`
	Withdraw      int32 `json:"withdraw"`
	Personal_info int32 `json:"personal_info"`
	IdInfo        int32 `json:"id_info"`
}

type Infomation struct {
	Funds      Moneies `json:"funds"`
	Deposit    Moneies `json:"deposit"`
	Rights     Rights  `json:"rights"`
	OpenOrders int32   `json:"open_orders"`
	ServerTime int64   `json:"server_time"`
}
type BalanceSetting struct {
	Balance Moneies `json:"balance"`
	Diff    Moneies `json:"diff"`
}

func NewInfomation(jsonBytes []byte) (Infomation, error) {
	result := ResultInfomation{}
	if err := json.Unmarshal(jsonBytes, &result); err != nil {
		return Infomation{}, err
	}
	if result.Success != 1 {
		return Infomation{}, errors.New(result.Error)
	}
	data := result.Return
	return data, nil
}

func GetInfomation() (Infomation, error) {
	nonce := getNonce()
	body := fmt.Sprintf("nonce=%.9f&method=get_info2", nonce)
	req := createReqest(body)
	out, err := SendRequest(req)
	if err != nil {
		log.Printf("GetInfomation: %s", err)
		return Infomation{}, err
	}

	return NewInfomation(out)
}

func NewBalanceSetting(jsonBytes []byte) (BalanceSetting, error) {
	result := BalanceSetting{}
	if err := json.Unmarshal(jsonBytes, &result); err != nil {
		fmt.Printf("err:%s", err)
		return BalanceSetting{}, err
	}
	return result, nil
}

func CheckBalance(info Infomation, balanceSetting BalanceSetting) Moneies {
	m := Moneies{}
	for key, param := range balanceSetting.Balance {
		data := info.Deposit[key] - param
		diff := math.Abs(float64(data))
		if balanceSetting.Diff[key] == 0 {
		} else if diff > balanceSetting.Diff[key] {
			m[key] = data
		} else {
		}
	}
	return m
}
