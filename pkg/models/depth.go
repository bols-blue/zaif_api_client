// All return values from Bitbank.cc is set in this package models.
package models

import (
	"encoding/json"
	"sort"
)

// Array of [price] [quontity]
type DepthData struct {
	Asks [][]json.Number `json:"asks"`
	Bids [][]json.Number `json:"bids"`
}

// Return Asks in type of float64
func (p *DepthData) GetAsksFloat64() [][]float64 {
	var asksFloat64 [][]float64
	for _, ask := range p.Asks {
		a, _ := ask[0].Float64()
		b, _ := ask[1].Float64()
		asksFloat64 = append(asksFloat64, []float64{a, b})
	}
	return asksFloat64
}

// Return Bids in type of float64
func (p *DepthData) GetBidsFloat64() [][]float64 {
	var bidsFloat64 [][]float64
	for _, bid := range p.Bids {
		a, _ := bid[0].Float64()
		b, _ := bid[1].Float64()
		bidsFloat64 = append(bidsFloat64, []float64{a, b})
	}
	return bidsFloat64
}

// Return Sorted(Quontity asc) Asks in type of float64
func (p *DepthData) SortAsksByQuontity() [][]float64 {
	sortAsks := p.GetAsksFloat64()
	sort.Slice(sortAsks, func(i, j int) bool {
		return sortAsks[i][1] > sortAsks[j][1]
	})
	return sortAsks
}

// Return Sorted(Quontity asc) Bid in type of float64
func (p *DepthData) SortBidsByQuontity() [][]float64 {
	sortBids := p.GetBidsFloat64()
	sort.Slice(sortBids, func(i, j int) bool {
		return sortBids[i][1] > sortBids[j][1]
	})
	return sortBids
}

// Return Asks in type of float64, Sorted by Price.  you can set order with "asc" or "desc".
func (p *DepthData) SortAsksByPrice(order string) [][]float64 {
	sortAsks := p.GetAsksFloat64()
	if order == "asc" {
		sort.Slice(sortAsks, func(i, j int) bool {
			return sortAsks[i][0] < sortAsks[j][0]
		})
	} else {
		sort.Slice(sortAsks, func(i, j int) bool {
			return sortAsks[i][0] > sortAsks[j][0]
		})
	}
	return sortAsks
}

// Return Bids in type of float64, Sorted by Price.  you can set order with "asc" or "desc".
func (p *DepthData) SortBidsByPrice(order string) [][]float64 {
	sortBids := p.GetBidsFloat64()
	if order == "asc" {
		sort.Slice(sortBids, func(i, j int) bool {
			return sortBids[i][0] < sortBids[j][0]
		})
	} else {
		sort.Slice(sortBids, func(i, j int) bool {
			return sortBids[i][0] > sortBids[j][0]
		})
	}
	return sortBids
}
