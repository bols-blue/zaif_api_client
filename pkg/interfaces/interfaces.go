package interfaces

import "gitlab.com/bols-blue/zaif_api_client/pkg/currency"

type Order interface {
	IsNoOrder() bool
	CancelOrder()
	ClearOrder()
	CreateOrder(set currency.CurrencySet, name string, price float64, tradeType string) Order
	CreateRawOrder(unit float64, name string, price float64, tradeType string, comment string) Order
	String() string
	GetPrice() float64
	IsTooNew() bool
	UpdateOrderData(unit float64, name string, price float64, tradeType string, comment string)
	SetID(int64)
	GetID() int64
	CancelOrderFromID(id int64, name string)
}
